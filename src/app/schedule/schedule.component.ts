import { Location } from '@angular/common';
import { isGeneratedFile } from '@angular/compiler/src/aot/util';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbTimepicker } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from '../shared/services/modal.service'; import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { of } from 'rxjs';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {
  formFrom: FormGroup;
  formTo: FormGroup;
  hours: any[] = [];
  minutes: any[] = [];
  chosenDay: string = '';

  scheduleOptions = [
    { text: "Monday", enabled: false },
    { text: "Tuesday", enabled: false },
    { text: "Wednesday ", enabled: false },
    { text: "Thursday", enabled: false },
    { text: "Friday", enabled: false },
    { text: "Saturday", enabled: false },
    { text: "Sunday", enabled: false },
  ]

  constructor(public location: Location, private modalService: ModalService, private formBuilder: FormBuilder) { }

  async ngOnInit() {
    await this.createDropdowns()
    this.formFrom = this.formBuilder.group({
      hours: [''],
      minutes: ['']
    });

    this.formTo = this.formBuilder.group({
      hours: [''],
      minutes: ['']
    });
  }

  open(opts?) {
    // this.modalService.open(opts);
    if (opts) {
      console.log(opts)
      this.chosenDay = opts.text;
    }
  }

  submit() {
    this.scheduleOptions.map((day) => {
      if (day.text == this.chosenDay) {
        day.enabled = true;
      }
    })
    this.chosenDay = '';
  }

  createDropdowns() {
    for (let i = 0; i < 24; i++) {
      this.hours.push({ id: i.toString(), name: i < 10 ? `0${i}` : i.toString() });
    }
    for (let i = 0; i < 60; i++) {
      this.minutes.push({ id: i.toString(), name: i < 10 ? `0${i}` : i.toString() });
    }
  }

  clear(){
    this.scheduleOptions.map(day=>{
      day.enabled =false;
    })
  }
}
