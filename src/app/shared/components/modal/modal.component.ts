import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ModalService } from "../../services/modal.service"

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  // time = {hour: 13, minute: 30};
  form: FormGroup;
  public opts;
  display$: Observable<'open' | 'close'>;


  constructor(private modalService: ModalService, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
    });
  }

  ngOnInit() {
    this.display$ = this.modalService.watch();
  }

  close() {
    this.modalService.close();
  }

  submit() {
    console.log(this.form.value);
  }

}
