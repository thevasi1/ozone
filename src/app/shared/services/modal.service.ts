import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private display: BehaviorSubject<'open' | 'close'> = new BehaviorSubject('close');
  private opts;

  watch(): Observable<'open' | 'close'> {
    return this.display.asObservable();
  }

  open(opts?) {
    if (opts) {
      this.opts = opts;
    } else {
      this.opts = null;
    }
    this.display.next('open');
  }

  close() {
    this.display.next('close');
  }

  getOpts() {
    return this.opts;
  }
}
