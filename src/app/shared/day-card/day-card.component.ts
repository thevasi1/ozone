import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'day-card',
  templateUrl: './day-card.component.html',
  styleUrls: ['./day-card.component.css']
})
export class DayCardComponent implements OnInit {
  @Input() text:string;
  @Input() enabled:boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
