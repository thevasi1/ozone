import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DayCardComponent } from './day-card/day-card.component';
import { ModalComponent } from './components/modal/modal.component';

@NgModule({
  declarations: [DayCardComponent, ModalComponent],
  imports: [CommonModule],
  exports: [DayCardComponent,ModalComponent],
  schemas: [NO_ERRORS_SCHEMA],
  providers:[]
})
export class SharedModule { }
