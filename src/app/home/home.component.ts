import { Component, OnInit } from '@angular/core';
import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';
import { EventMqttService } from "../shared/services/event-mqtt.service"

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  events: any[];
  private deviceId: string;
  subscription: Subscription;

  constructor(private readonly eventMqtt: EventMqttService, private mqttService: MqttService) { }

  ngOnInit() {
    this.subscribeToTopic();
  }

  doSpray() {
    let result = this.mqttService.unsafePublish("dd76bee806f0f2868b4b1d5e61dff3d7cf59ebba3b093ac0fdf380f0d4cf4647", "Prusni", { qos: 1, retain: false })
  }

  private subscribeToTopic() {
    this.subscription = this.eventMqtt.topic(this.deviceId)
      .subscribe((data: IMqttMessage) => {
        let item = JSON.parse(data.payload.toString());
        this.events.push(item);
      });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}