export const environment = {
  production: true,
	hmr: false,
	// http: {
	// 	apiUrl: '<https://api.myweb.com>',
	// },
	mqtt: {
		server: 'mqtt.eclipse.org/mqtt',
		protocol: "wss",
		port: 80
	}
};
