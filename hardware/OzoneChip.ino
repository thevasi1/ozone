/*
 Basic MQTT example 
 
  - connects to an MQTT server
  - publishes "hello world" to the topic "outTopic"
  - subscribes to the topic "inTopic"
*/
#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>

#define RELAY_PIN 13
#define BUFFER_SIZE 100

const char *ssid =	"vleztuk";		// cannot be longer than 32 characters!
const char *pass =	"hackhack";		//
bool squirted = 0;
bool shooting = false;
// Update these with values suitable for your network.
const long utcOffsetInSeconds = 7200;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

IPAddress server(137, 135, 83, 217);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

struct Shoot {
  char* Day = daysOfTheWeek[timeClient.getDay()];
  long Minute = timeClient.getMinutes();
  long Second = timeClient.getSeconds();
  long Hour = timeClient.getHours();
};
long interval = 30;
Shoot ls; // Last shot
Shoot ns; // Next
void callback(const MQTT::Publish& pub) {
  //shoot();
  Serial.print(pub.topic());
  Serial.print(" => ");
  if (pub.has_stream()) {
    uint8_t buf[BUFFER_SIZE];
    int read;
    
    while (read = pub.payload_stream()->read(buf, BUFFER_SIZE)) {
      Serial.write(buf, read);
    }
    pub.payload_stream()->stop();
    Serial.println("");
  } else {
    if(pub.payload_string() == "Prusni" && !shooting) {
      shooting = true;
      shoot();
    }
    Serial.println(pub.payload_string());
  }
}

WiFiClient wclient;
PubSubClient client(wclient, server);

void shoot() {
   digitalWrite(RELAY_PIN, HIGH);
   delay(600);
   digitalWrite(RELAY_PIN, LOW);
   ls.Day = daysOfTheWeek[timeClient.getDay()];
   ls.Minute = timeClient.getMinutes();
   ls.Second = timeClient.getSeconds();
   ls.Hour = timeClient.getHours();
   ns.Day = ls.Day;
   ns.Minute = ls.Minute;
   ns.Second = ls.Second;
   ns.Hour = ls.Hour;
   shooting = false;
   ns.Second += interval;
   if (ns.Second >= 60){  
    ns.Minute ++;
    ns.Second = 1;
    if (ns.Minute >= 60){
      ns.Hour ++;
      ns.Minute = 0;
    }
    
   }
   
}

void setup() {
  // Setup console
  timeClient.begin();
  pinMode(RELAY_PIN, OUTPUT);
  Serial.begin(115200);
  delay(10);
  Serial.println();
  Serial.println();
  
  client.set_callback(callback);
}

void loop() {
  timeClient.update();
  if (daysOfTheWeek[timeClient.getDay()] == ns.Day && timeClient.getHours() == ns.Hour && timeClient.getMinutes() == ns.Minute && timeClient.getSeconds() == ns.Second){
    //shoot();
    //shooting = true;
  }
  Serial.print(daysOfTheWeek[timeClient.getDay()]);
  Serial.print(", ");
  Serial.print(timeClient.getHours());
  Serial.print(":");
  Serial.print(timeClient.getMinutes());
  Serial.print(":");
  
  Serial.print(timeClient.getSeconds());
  Serial.print("            ");
  Serial.print(ns.Hour);
  Serial.print(" ");
  Serial.print(ns.Minute);
  Serial.print(" ");
  Serial.print(ns.Second);
  Serial.println();
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("Connecting to ");
    Serial.print(ssid);
    Serial.println("...");
    WiFi.begin(ssid, pass);

    if (WiFi.waitForConnectResult() != WL_CONNECTED)
      return;
    Serial.println("WiFi connected");
  }
  
  if (WiFi.status() == WL_CONNECTED) {
    if (!client.connected()) {
      if (client.connect("arduinoClient")) {
	      
	      client.subscribe("dd76bee806f0f2868b4b1d5e61dff3d7cf59ebba3b093ac0fdf380f0d4cf4647");

        //client.publish("dd76bee806f0f2868b4b1d5e61dff3d7cf59ebba3b093ac0fdf380f0d4cf4647","Test");
      }
    }

    if (client.connected()) {
      //Serial.println("Connected");
      client.loop();
    
    }
  }
  
}
